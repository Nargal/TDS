// Copyright Elin Denis. All Rights Reserved.
// Developed with Unreal Engine 5.0
using UnrealBuildTool;

public class TDS : ModuleRules
{
    public TDS(ReadOnlyTargetRules Target) : base(Target)
    {
        PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicIncludePaths.AddRange(new string[]
        {
            "TDS"
        });

        PublicDependencyModuleNames.AddRange(new string[] {
            "Core",
            "CoreUObject",
            "Engine",
            "InputCore",
            "Niagara",
            "HeadMountedDisplay",
            "NavigationSystem",
            "AIModule",
            "PhysicsCore",
            "SlateCore",
            "Slate"
        });

        PrivateDependencyModuleNames.AddRange(new string[] { });
    }
}