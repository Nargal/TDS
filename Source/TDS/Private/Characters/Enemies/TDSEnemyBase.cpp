// Copyright Elin Denis. All Rights Reserved.
// Developed with Unreal Engine 5.0
#include "Characters/Enemies/TDSEnemyBase.h"

// Engine class include
#include "Engine/ActorChannel.h"
#include "Particles/ParticleSystemComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Net/UnrealNetwork.h"

// Project class include
#include "StateEffects/TDS_StateEffect.h"

ATDSEnemyBase::ATDSEnemyBase()
{
    PrimaryActorTick.bCanEverTick = true;
    bReplicates = true;
}

void ATDSEnemyBase::BeginPlay()
{
    Super::BeginPlay();
}

void ATDSEnemyBase::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
}

bool ATDSEnemyBase::ReplicateSubobjects(UActorChannel* Channel, FOutBunch* Bunch, FReplicationFlags* RepFlags)
{
    bool Wrote = Super::ReplicateSubobjects(Channel, Bunch, RepFlags);

    for (int32 i = 0; i < Effects.Num(); i++)
    {
        if (Effects.IsValidIndex(i) && Effects[i])
        {
            Wrote |= Channel->ReplicateSubobject(Effects[i], *Bunch,
                *RepFlags);  // (this makes those subobjects 'supported', and from here on those objects may have reference replicated)
        }
    }

    return Wrote;
}

void ATDSEnemyBase::RemoveEffect_Implementation(UTDS_StateEffect* RemoveEffect)
{
    Effects.Remove(RemoveEffect);

    if (!RemoveEffect->bIsAutoDestroyEffect)
    {
        SwitchEffect(RemoveEffect, false);
        EffectRemove = RemoveEffect;
    }
}

void ATDSEnemyBase::AddEffect_Implementation(UTDS_StateEffect* NewEffect)
{
    Effects.Add(NewEffect);

    if (!NewEffect->bIsAutoDestroyEffect)
    {
        SwitchEffect(NewEffect, true);
        EffectAdd = NewEffect;
    }
    else
    {
        if (NewEffect->ParticleEffect)
        {
            ExecuteEffectAdded_OnServer(NewEffect->ParticleEffect);
        }
    }
}

void ATDSEnemyBase::EffectAdd_OnRep()
{
    if (!EffectAdd) return;
    SwitchEffect(EffectAdd, true);
}

void ATDSEnemyBase::EffectRemove_OnRep()
{
    if (!EffectRemove) return;
    SwitchEffect(EffectRemove, false);
}

void ATDSEnemyBase::SwitchEffect(UTDS_StateEffect* Effect, bool bIsAdd)
{
    if (bIsAdd)
    {
        if (Effect && Effect->ParticleEffect)
        {
            FName NameBoneToAttached = Effect->NameBone;
            FVector Location = FVector::ZeroVector;

            const auto SkeletalMeshComponent = GetMesh();
            if (SkeletalMeshComponent)
            {
                const auto PSystemComponent = UGameplayStatics::SpawnEmitterAttached(Effect->ParticleEffect, SkeletalMeshComponent,
                    NameBoneToAttached, Location, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
                ParticleSystemEffects.Add(PSystemComponent);
            }
        }
    }
    else
    {
        int32 i = 0;
        bool bIsFind = false;
        if (ParticleSystemEffects.Num() > 0)
        {
            while (i < ParticleSystemEffects.Num() && !bIsFind)
            {
                if (ParticleSystemEffects[i]->Template && Effect->ParticleEffect &&
                    Effect->ParticleEffect == ParticleSystemEffects[i]->Template)
                {
                    bIsFind = true;
                    ParticleSystemEffects[i]->DeactivateSystem();
                    ParticleSystemEffects[i]->DestroyComponent();
                    ParticleSystemEffects.RemoveAt(i);
                }

                ++i;
            }
        }
    }
}

void ATDSEnemyBase::ExecuteEffectAdded_OnServer_Implementation(UParticleSystem* ExecuteFX)
{
    ExecuteEffectAdded_Multicast(ExecuteFX);
}

void ATDSEnemyBase::ExecuteEffectAdded_Multicast_Implementation(UParticleSystem* ExecuteFX)
{
    UTDSTypes::ExecuteEffectAdded(ExecuteFX, this, FVector::ZeroVector, FName("Spine_01"));
}

void ATDSEnemyBase::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);

    DOREPLIFETIME(ATDSEnemyBase, Effects);
    DOREPLIFETIME(ATDSEnemyBase, EffectAdd);
    DOREPLIFETIME(ATDSEnemyBase, EffectRemove);
}