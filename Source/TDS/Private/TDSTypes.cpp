// Copyright Elin Denis. All Rights Reserved.
// Developed with Unreal Engine 5.0
#include "TDSTypes.h"

// Engine class include
#include "GameFramework/Character.h"
#include "Kismet/GameplayStatics.h"

// Project class include
#include "TDS.h"
#include "StateEffects/TDS_StateEffect.h"
#include "Interfaces/TDS_IGameActor.h"

void UTDSTypes::AddEffectBySurfaceType(
    AActor* TakeEffectActor, FName NameBoneHit, TSubclassOf<UTDS_StateEffect> AddEffectClass, EPhysicalSurface PhysicalSurface)
{
    if (PhysicalSurface != EPhysicalSurface::SurfaceType_Default && AddEffectClass && TakeEffectActor)
    {
        UTDS_StateEffect* MyEffect = Cast<UTDS_StateEffect>(AddEffectClass->GetDefaultObject());
        if (MyEffect)
        {
            bool bIsHavePossibleSurface = false;
            int8 i = 0;
            while (i < MyEffect->PossibleInteractSurface.Num() && !bIsHavePossibleSurface)
            {
                if (MyEffect->PossibleInteractSurface[i] == PhysicalSurface)
                {
                    bIsHavePossibleSurface = true;

                    bool bIsCanAddEffect = false;
                    if (!MyEffect->bIsStakable)
                    {
                        TArray<UTDS_StateEffect*> CurrentEffects;
                        ITDS_IGameActor* Interface = Cast<ITDS_IGameActor>(TakeEffectActor);
                        if (Interface)
                        {
                            CurrentEffects = Interface->GetAllCurrentEffects();
                        }

                        if (CurrentEffects.Num() > 0)
                        {
                            int8 j = 0;
                            while (j < CurrentEffects.Num() && !bIsCanAddEffect)
                            {
                                if (CurrentEffects[j]->GetClass() != AddEffectClass)
                                {
                                    bIsCanAddEffect = true;
                                }

                                j++;
                            }
                        }
                        else
                        {
                            bIsCanAddEffect = true;
                        }
                    }
                    else
                    {
                        bIsCanAddEffect = true;
                    }

                    if (bIsCanAddEffect)
                    {
                        UTDS_StateEffect* NewEffect = NewObject<UTDS_StateEffect>(TakeEffectActor, AddEffectClass);
                        if (NewEffect)
                        {
                            NewEffect->InitObject(TakeEffectActor, NameBoneHit);
                        }
                    }
                }
                i++;
            }
        }
    }
}

void UTDSTypes::ExecuteEffectAdded(UParticleSystem* ExecuteFX, AActor* Target, FVector Offcet, FName Socket)
{
    if (Target)
    {
        const FName SocketToAttached = Socket;
        const FVector Location = Offcet;

        const ACharacter* Character = Cast<ACharacter>(Target);
        if (Character && Character->GetMesh())
        {
            UGameplayStatics::SpawnEmitterAttached(
                ExecuteFX, Character->GetMesh(), SocketToAttached, Location, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
        }
        else
        {
            UGameplayStatics::SpawnEmitterAttached(ExecuteFX, Target->GetRootComponent(), SocketToAttached, Location, FRotator::ZeroRotator,
                EAttachLocation::SnapToTarget, false);
        }
    }
}