// Copyright Elin Denis. All Rights Reserved.
// Developed with Unreal Engine 5.0
#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "TDSTypes.h"
#include "InventoryComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(
    FOnSwitchWeaponSignature, FName, WeaponID_Name, FAdditionalWeaponData, WeaponAdditionalInfo, int32, NewWeaponIndex);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnAmmoChangeSignature, EWeaponType, AmmoType, int32, Cout);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(
    FOnWeaponAdditionalInfoChangeSignature, int32, IndexSlot, FAdditionalWeaponData, AdditionalInfo);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponAmmoAviableSignature, EWeaponType, WeaponType);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponAmmoEmptySignature, EWeaponType, WeaponType);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnUpdateWeaponSlotsSignature, int32, IndexSlotChange, FWeaponSlotData, NewInfo);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponNotHaveRoundSignature, int32, IndexSlotWeapon);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponHaveRoundSignature, int32, IndexSlotWeapon);

class ATDSWeaponPickup;

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class TDS_API UInventoryComponent : public UActorComponent
{
    GENERATED_BODY()

public:
    UInventoryComponent();

    UPROPERTY(BlueprintAssignable, Category = "Inventory")
    FOnSwitchWeaponSignature OnSwitchWeapon;

    UPROPERTY(BlueprintAssignable, Category = "Inventory")
    FOnAmmoChangeSignature OnAmmoChange;

    UPROPERTY(BlueprintAssignable, Category = "Inventory")
    FOnWeaponAdditionalInfoChangeSignature OnWeaponAdditionalInfo;

    UPROPERTY(BlueprintAssignable, Category = "Inventory")
    FOnWeaponAmmoAviableSignature OnWeaponAmmoAviable;

    UPROPERTY(BlueprintAssignable, Category = "Inventory")
    FOnWeaponAmmoEmptySignature OnWeaponAmmoEmpty;

    UPROPERTY(BlueprintAssignable, Category = "Inventory")
    FOnUpdateWeaponSlotsSignature OnUpdateWeaponSlots;

    // Event current weapon not have additional_Rounds
    UPROPERTY(BlueprintAssignable, Category = "Inventory")
    FOnWeaponNotHaveRoundSignature OnWeaponNotHaveRound;

    // Event current weapon have additional_Rounds
    UPROPERTY(BlueprintAssignable, Category = "Inventory")
    FOnWeaponHaveRoundSignature OnWeaponHaveRound;

protected:
    virtual void BeginPlay() override;

private:
    UPROPERTY(Replicated, EditDefaultsOnly, Category = "Inventory")
    TArray<FWeaponSlotData> WeaponSlots;

    UPROPERTY(Replicated, EditDefaultsOnly, Category = "Inventory")
    TArray<FAmmoSlotData> AmmoSlots;

    int32 MaxSlotWeapon = 0;

public:
    bool SwitchWeaponToIndex(int32 ChangeToIndex, int32 OldIndex, FAdditionalWeaponData OldInfo, bool bIsForward);
    bool SwitchWeaponByIndex(int32 IndexWeaponToChange, int32 PreviosIndex, FAdditionalWeaponData PreviosWeaponInfo);

    void SetAdditionalInfoWeapon(int32 IndexWeapon, FAdditionalWeaponData NewInfo);

    FAdditionalWeaponData GetAdditionalInfoWeapon(int32 IndexWeapon);
    int32 GetWeaponIndexSlotByName(FName WeaponID_Name);
    FName GetWeaponNameBySlotIndex(int32 IndexSlot) const;

    bool GetWeaponTypeByIndexSlot(int32 IndexSlot, EWeaponType& WeaponType);
    bool GetWeaponTypeByNameWeapon(FName WeaponID_Name, EWeaponType& WeaponType);

    UFUNCTION(BlueprintCallable)
    void AmmoSlotChangeValue(EWeaponType WeaponType, int32 CoutChangeAmmo);

    bool CheckAmmoForWeapon(EWeaponType WeaponType, int32& AviableAmmoForWeapon);

    // Interface PickUp Actors
    UFUNCTION(BlueprintCallable, Category = "Interface")
    bool CheckCanTakeAmmo(EWeaponType AmmoType);

    // Interface PickUp Actors
    UFUNCTION(BlueprintCallable, Category = "Interface")
    bool CheckCanTakeWeapon(int32& FreeSlot);

    UFUNCTION(BlueprintCallable, Category = "Interface")
    bool SwitchWeaponToInventory(FWeaponSlotData NewWeapon, int32 IndexSlot, int32 CurrentIndexWeaponChar, FDropItemData& DropItemInfo);

    UFUNCTION(BlueprintCallable, Category = "Interface")
    bool GetDropItemInfoFromInventory(int32 IndexSlot, FDropItemData& DropItemInfo);

    UFUNCTION(Server, Reliable, BlueprintCallable, Category = "Interface")
    void TryGetWeaponToInventory_OnServer(AActor* PickupActor, FWeaponSlotData NewWeapon);

    UFUNCTION(Server, Reliable, BlueprintCallable, Category = "Interface")
    void DropWeaponByIndex_OnServer(int32 ByIndex);

    UFUNCTION(BlueprintCallable, Category = "Inventory")
    TArray<FWeaponSlotData> GetWeaponSlotsData() const;

    UFUNCTION(BlueprintCallable, Category = "Inventory")
    TArray<FAmmoSlotData> GetAmmoSlotsData() const;

    UFUNCTION(Server, Reliable, BlueprintCallable, Category = "Inventory")
    void InitInventory_OnServer(const TArray<FWeaponSlotData>& NewWeaponSlotsData, const TArray<FAmmoSlotData>& NewAmmoSlotsData);

    UFUNCTION(NetMulticast, Reliable, BlueprintCallable, Category = "Inventory")
    void AmmoChangeEvent_Multicast(EWeaponType WeaponType, int32 Cout);

    UFUNCTION(Server, Reliable, BlueprintCallable, Category = "Inventory")
    void SwitchWeaponEvent_OnServer(FName WeaponName, FAdditionalWeaponData AdditionalWeaponData, int32 IndexSlot);

    UFUNCTION(NetMulticast, Reliable, BlueprintCallable, Category = "Inventory")
    void WeaponAdditionalInfoChangeEvent_Multicast(int32 IndexSlot, FAdditionalWeaponData AdditionalWeaponData);

    UFUNCTION(NetMulticast, Reliable, BlueprintCallable, Category = "Inventory")
    void WeaponAmmoAviableEvent_Multicast(EWeaponType WeaponType);

    UFUNCTION(NetMulticast, Reliable, BlueprintCallable, Category = "Inventory")
    void WeaponAmmoEmptyEvent_Multicast(EWeaponType WeaponType);

    UFUNCTION(NetMulticast, Reliable, BlueprintCallable, Category = "Inventory")
    void UpdateWeaponSlotsEvent_Multicast(int32 IndexSlotChange, FWeaponSlotData WeaponSlotData);

    UFUNCTION(NetMulticast, Reliable, BlueprintCallable, Category = "Inventory")
    void WeaponNotHaveRoundEvent_Multicast(int32 IndexSlotWeapon);

    UFUNCTION(NetMulticast, Reliable, BlueprintCallable, Category = "Inventory")
    void WeaponHaveRoundEvent_Multicast(int32 IndexSlotWeapon);
};