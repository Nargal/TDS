// Copyright Elin Denis. All Rights Reserved.
// Developed with Unreal Engine 5.0
#include "Weapons/WeaponBase.h"

// Engine class include
#include "DrawDebugHelpers.h"
#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystemComponent.h"
#include "Sound/SoundCue.h"
#include "NiagaraComponent.h"
#include "NiagaraFunctionLibrary.h"
#include "Net/UnrealNetwork.h"

// Project class include
#include "Weapons/Items/DropShell.h"
#include "StateEffects/TDS_StateEffect.h"
#include "Components/InventoryComponent.h"
#include "TDSUtils.h"

int32 DebugWeaponShow = 0;
FAutoConsoleVariableRef CVarWeaponShow(TEXT("TDS.DebugWeapon"), DebugWeaponShow, TEXT("Drow Debug for Weapon"), ECVF_Cheat);

DEFINE_LOG_CATEGORY_STATIC(LogWeaponBase, All, All);

AWeaponBase::AWeaponBase()
{
    PrimaryActorTick.bCanEverTick = true;

    SetReplicates(true);

    SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
    RootComponent = SceneComponent;

    SkeletalMeshWeapon = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Skeletal Mesh"));
    SkeletalMeshWeapon->SetGenerateOverlapEvents(false);
    SkeletalMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
    SkeletalMeshWeapon->SetupAttachment(RootComponent);

    StaticMeshWeapon = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh"));
    StaticMeshWeapon->SetGenerateOverlapEvents(false);
    StaticMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
    StaticMeshWeapon->SetupAttachment(RootComponent);

    ShootLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("ShootLocation"));
    ShootLocation->ArrowSize = 0.5f;
    ShootLocation->SetupAttachment(RootComponent);

    SleeveDropLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("SleeveDropLocation"));
    SleeveDropLocation->ArrowSize = 0.2f;
    SleeveDropLocation->ArrowColor = FColor::Emerald;
    SleeveDropLocation->SetupAttachment(RootComponent);

    ClipDropLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("ClipDropLocation"));
    ClipDropLocation->ArrowSize = 0.2f;
    ClipDropLocation->ArrowColor = FColor::Yellow;
    ClipDropLocation->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void AWeaponBase::BeginPlay()
{
    Super::BeginPlay();

    WeaponInit();

    /** For Debug */
    if (bFireAtStartGame)
    {
        bWeaponFiring = true;
    }
}

// Called every frame
void AWeaponBase::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);

    if (HasAuthority())
    {
        FireTick(DeltaTime);
        ReloadTick(DeltaTime);
        DispersionTick(DeltaTime);
        DropMeshTick(DeltaTime);
    }
}

void AWeaponBase::FireTick(float DeltaTime)
{
    if (FireTimer <= 0.f)
    {
        FireTimer = 0.0f;
        if (bWeaponFiring && GetWeaponRound() > 0 && !bWeaponReloading)
        {
            Fire();
        }
    }
    else
    {
        FireTimer -= DeltaTime;
    }
}

void AWeaponBase::ReloadTick(float DeltaTime)
{
    if (bWeaponReloading)
    {
        if (ReloadTimer < 0.0f)
            FinishReload();
        else
            ReloadTimer -= DeltaTime;
    }
}

void AWeaponBase::DispersionTick(float DeltaTime)
{
    if (!bWeaponReloading)
    {
        if (!bWeaponFiring)
        {
            if (bShouldReduceDispersion)
                CurrentDispersion = CurrentDispersion - CurrentDispersionReduction;
            else
                CurrentDispersion = CurrentDispersion + CurrentDispersionReduction;
        }

        if (CurrentDispersion < CurrentDispersionMin)
            CurrentDispersion = CurrentDispersionMin;
        else if (CurrentDispersion > CurrentDispersionMax)
            CurrentDispersion = CurrentDispersionMax;
    }

    if (DebugWeaponShow)
        UE_LOG(LogWeaponBase, Warning, TEXT("Dispersion: MAX = %f. MIN = %f. Current = %f"), CurrentDispersionMax, CurrentDispersionMin,
            CurrentDispersion);
}

void AWeaponBase::DropMeshTick(float DeltaTime)
{
    if (bDropSleeve)
    {
        if (DropSleeveTimer < 0.0f)
        {
            bDropSleeve = false;
            InitDropMesh(WeaponSettings.DropSleeve, SleeveDropLocation);
        }
        else
            DropSleeveTimer -= DeltaTime;
    }

    if (bDropClip)
    {
        if (DropClipTimer < 0.0f)
        {
            bDropClip = false;
            InitDropMesh(WeaponSettings.DropClip, ClipDropLocation);
        }
        else
            DropClipTimer -= DeltaTime;
    }
}

void AWeaponBase::WeaponInit()
{
    if (SkeletalMeshWeapon && !SkeletalMeshWeapon->SkeletalMesh) SkeletalMeshWeapon->DestroyComponent(true);

    if (StaticMeshWeapon && !StaticMeshWeapon->GetStaticMesh()) StaticMeshWeapon->DestroyComponent();

    UpdateStateWeapon_OnServer(EMovementState::RUN_STATE);
}

void AWeaponBase::SetWeaponStateFire_OnServer_Implementation(bool bIsFire)
{
    if (CheckWeaponCanFire())
        bWeaponFiring = bIsFire;
    else
        bWeaponFiring = false;
}

void AWeaponBase::Fire()
{
    // On Server by weapon fire bool

    UAnimMontage* CharAnimToPlay = nullptr;
    if (bAiming)
        CharAnimToPlay = WeaponSettings.CharacterAnimation.CharacterFireIronsights;
    else
        CharAnimToPlay = WeaponSettings.CharacterAnimation.CharacterFireHip;

    if (WeaponSettings.WeaponAnimation.WeaponFire)
    {
        AnimWeaponStart_Multicast(WeaponSettings.WeaponAnimation.WeaponFire);
    }

    if (WeaponSettings.DropSleeve.DropItemClass)
    {
        if (WeaponSettings.DropSleeve.DropDelay < 0.0f)
            InitDropMesh(WeaponSettings.DropSleeve, SleeveDropLocation);
        else
        {
            bDropSleeve = true;
            DropSleeveTimer = WeaponSettings.DropSleeve.DropDelay;
        }
    }

    FireTimer = WeaponSettings.RateOfFire;
    AdditionalWeaponInfo.Round = AdditionalWeaponInfo.Round - 1;
    ChangeDispersionByShot();

    OnWeaponFireStart.Broadcast(CharAnimToPlay);

    WeaponFireFX_Multicast(WeaponSettings.WeaponEffects.FireEffect, WeaponSettings.SoundFireWeapon);

    int8 NumberProjectile = GetNumberProjectileByShot();

    if (ShootLocation)
    {
        FVector SpawnLocation = ShootLocation->GetComponentLocation();
        FRotator SpawnRotation = ShootLocation->GetComponentRotation();

        FFireData FireData = WeaponSettings.FireData;

        FVector EndLocation;
        for (int8 i = 0; i < NumberProjectile; i++)
        {
            EndLocation = GetFireEndLocation();
            if (FireData.bUseProjectile && FireData.ProjectileData.ProjectileClass)
            {
                // Projectile Init ballistic fire
                FVector Dir = EndLocation - SpawnLocation;
                Dir.Normalize();

                FMatrix MyMatrix(Dir, FVector(0.0f, 1.0f, 0.0f), FVector(0.0f, 0.0f, 1.0f), FVector::ZeroVector);
                SpawnRotation = MyMatrix.Rotator();

                FTransform SpawnTransform(FQuat(SpawnRotation), SpawnLocation);
                AProjectile* Projectile =
                    GetWorld()->SpawnActorDeferred<AProjectile>(FireData.ProjectileData.ProjectileClass, SpawnTransform);
                if (Projectile)
                {
                    Projectile->SpawnCollisionHandlingMethod = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
                    Projectile->SetOwner(GetOwner());
                    Projectile->SetInstigator(GetInstigator());

                    Projectile->InitProjectile(FireData);
                    Projectile->FinishSpawning(SpawnTransform);
                }
            }
            else
            {
                FHitResult Hit;
                TArray<AActor*> Actors;

                EDrawDebugTrace::Type DebugTrace;
                if (DebugWeaponShow)
                {
                    DrawDebugLine(GetWorld(), SpawnLocation, SpawnLocation + EndLocation * FireData.TraceData.TraceDistacne, FColor::Black,
                        false, 5.0f, (uint8)'\000', 0.5f);
                    DebugTrace = EDrawDebugTrace::ForDuration;
                }
                else
                    DebugTrace = EDrawDebugTrace::None;

                UKismetSystemLibrary::LineTraceSingle(GetWorld(), SpawnLocation, EndLocation * FireData.TraceData.TraceDistacne,
                    ETraceTypeQuery::TraceTypeQuery4, false, Actors, DebugTrace, Hit, true, FLinearColor::Red, FLinearColor::Green, 5.0f);

                if (Hit.bBlockingHit)
                {
                    if (Hit.GetActor() && Hit.PhysMaterial.IsValid())
                    {
                        EPhysicalSurface mySurfacetype = UGameplayStatics::GetSurfaceType(Hit);
                        if (FireData.ImpactMapData.Contains(mySurfacetype))
                        {
                            const auto ImpactData = FireData.ImpactMapData[mySurfacetype];

                            UMaterialInterface* myMaterial = ImpactData.DecalData.Material;
                            if (myMaterial && Hit.GetComponent())
                            {
                                UGameplayStatics::SpawnDecalAttached(myMaterial, ImpactData.DecalData.Size, Hit.GetComponent(), NAME_None,
                                    Hit.ImpactPoint, Hit.ImpactNormal.Rotation(), EAttachLocation::KeepWorldPosition,
                                    ImpactData.DecalData.LifeTime);
                            }

                            if (ImpactData.HitEffect)
                            {
                                UParticleSystem* myParticle = ImpactData.HitEffect;
                                if (myParticle)
                                {
                                    UGameplayStatics::SpawnEmitterAtLocation(
                                        GetWorld(), myParticle, FTransform(Hit.ImpactNormal.Rotation(), Hit.ImpactPoint, FVector(1.0f)));
                                }
                            }

                            if (ImpactData.HitSound)
                            {
                                UGameplayStatics::PlaySoundAtLocation(GetWorld(), ImpactData.HitSound, Hit.ImpactPoint);
                            }

                            UTDSTypes::AddEffectBySurfaceType(Hit.GetActor(), Hit.BoneName, FireData.ProjectileData.Effect, mySurfacetype);
                        }
                    }

                    UGameplayStatics::ApplyPointDamage(Hit.GetActor(), FireData.ProjectileData.ProjectileDamage, Hit.TraceStart, Hit,
                        GetInstigatorController(), this, NULL);
                }

                if (FireData.TraceData.TraceEffect)
                {
                    const auto TraceFXComponent =
                        UNiagaraFunctionLibrary::SpawnSystemAtLocation(GetWorld(), FireData.TraceData.TraceEffect, SpawnLocation);
                    if (TraceFXComponent)
                    {
                        TraceFXComponent->SetNiagaraVariableVec3(FireData.TraceData.TraceTargetName, EndLocation);
                    }
                }
            }
        }
    }

    if (GetWeaponRound() <= 0 && !bWeaponReloading)
    {
        // Init Reload
        if (CheckCanWeaponReload()) InitReload();
    }
}

void AWeaponBase::UpdateStateWeapon_OnServer_Implementation(EMovementState NewMovementState)
{
    bBlockFire = false;

    switch (NewMovementState)
    {
        case EMovementState::AIMING_STATE:
            bAiming = true;
            CurrentDispersionMax = WeaponSettings.DispersionWeapon.AimingDispersionMin;
            CurrentDispersionMin = WeaponSettings.DispersionWeapon.AimingDispersionMax;
            CurrentDispersionRecoil = WeaponSettings.DispersionWeapon.AimingDispersionRecoil;
            CurrentDispersionReduction = WeaponSettings.DispersionWeapon.AimingDispersionReduction;
            break;
        case EMovementState::AIMING_WALK_STATE:
            bAiming = true;
            CurrentDispersionMin = WeaponSettings.DispersionWeapon.AimingWalkDispersionMin;
            CurrentDispersionMax = WeaponSettings.DispersionWeapon.AimingWalkDispersionMax;
            CurrentDispersionRecoil = WeaponSettings.DispersionWeapon.AimingWalkDispersionRecoil;
            CurrentDispersionReduction = WeaponSettings.DispersionWeapon.AimingWalkDispersionReduction;
            break;
        case EMovementState::WALK_STATE:
            bAiming = false;
            CurrentDispersionMin = WeaponSettings.DispersionWeapon.WalkDispersionMin;
            CurrentDispersionMax = WeaponSettings.DispersionWeapon.WalkDispersionMax;
            CurrentDispersionRecoil = WeaponSettings.DispersionWeapon.WalkDispersionRecoil;
            CurrentDispersionReduction = WeaponSettings.DispersionWeapon.WalkDispersionReduction;
            break;
        case EMovementState::RUN_STATE:
            bAiming = false;
            CurrentDispersionMax = WeaponSettings.DispersionWeapon.RunDispersionMin;
            CurrentDispersionMin = WeaponSettings.DispersionWeapon.RunDispersionMax;
            CurrentDispersionRecoil = WeaponSettings.DispersionWeapon.RunDispersionRecoil;
            CurrentDispersionReduction = WeaponSettings.DispersionWeapon.RunDispersionReduction;
            break;
        case EMovementState::SPRINT_STATE:
            bAiming = false;
            bBlockFire = true;
            SetWeaponStateFire_OnServer(false);
            break;
        default: break;
    }
}

FVector AWeaponBase::GetFireEndLocation() const
{
    bool bShootDirection = false;
    FVector EndLocation = FVector::ZeroVector;

    FVector tmpV = ShootLocation->GetComponentLocation() - ShootEndLocation;

    if (tmpV.Size() > SizeVectorToChangeShootDirectionLogic)
    {
        EndLocation = ShootLocation->GetComponentLocation() +
                      ApplyDispersionToShoot((ShootLocation->GetComponentLocation() - ShootEndLocation).GetSafeNormal()) * -20000.0f;
        if (DebugWeaponShow)
            DrawDebugCone(GetWorld(), ShootLocation->GetComponentLocation(), -(ShootLocation->GetComponentLocation() - ShootEndLocation),
                WeaponSettings.FireData.TraceData.TraceDistacne, GetCurrentDispersion() * PI / 180.f, GetCurrentDispersion() * PI / 180.f,
                32, FColor::Emerald, false, .1f, (uint8)'\000', 1.0f);
    }
    else
    {
        EndLocation = ShootLocation->GetComponentLocation() + ApplyDispersionToShoot(ShootLocation->GetForwardVector()) * 20000.0f;
        if (DebugWeaponShow)
            DrawDebugCone(GetWorld(), ShootLocation->GetComponentLocation(), ShootLocation->GetForwardVector(),
                WeaponSettings.FireData.TraceData.TraceDistacne, GetCurrentDispersion() * PI / 180.f, GetCurrentDispersion() * PI / 180.f,
                32, FColor::Emerald, false, .1f, (uint8)'\000', 1.0f);
    }

    if (DebugWeaponShow)
    {
        // Direction weapon look
        DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(),
            ShootLocation->GetComponentLocation() + ShootLocation->GetForwardVector() * 500.0f, FColor::Cyan, false, 5.f, (uint8)'\000',
            0.5f);
        // Direction projectile must fly
        DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), ShootEndLocation, FColor::Red, false, 5.f, (uint8)'\000', 0.5f);
        // Direction Projectile Current fly
        DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), EndLocation, FColor::Black, false, 5.f, (uint8)'\000', 0.5f);
    }

    return EndLocation;
}

void AWeaponBase::ChangeDispersionByShot()
{
    CurrentDispersion = CurrentDispersion + CurrentDispersionRecoil;
}

void AWeaponBase::InitReload()
{
    bWeaponReloading = true;
    ReloadTimer = WeaponSettings.ReloadTime;

    /** Fix RateOfFire timer */
    FireTimer -= WeaponSettings.ReloadTime;
    if (FireTimer < 0.0f)
    {
        FireTimer = 0.0f;
    }

    if (WeaponSettings.DropClip.DropItemClass)
    {
        if (WeaponSettings.DropClip.DropDelay < 0.0f)
        {
            InitDropMesh(WeaponSettings.DropClip, ClipDropLocation);
        }
        else
        {
            bDropClip = true;
            DropClipTimer = WeaponSettings.DropClip.DropDelay;
        }
    }

    PlayReloadFX();
}

void AWeaponBase::FinishReload()
{
    bWeaponReloading = false;

    int32 AmmoNeedTakeFromInv = 0;
    const int32 AvaiableAmmoInInventory = GetAviableAmmoForReload();
    const int32 NeedToReload = WeaponSettings.ClipSize - AdditionalWeaponInfo.Round;

    if (NeedToReload > AvaiableAmmoInInventory)
    {
        AdditionalWeaponInfo.Round += AvaiableAmmoInInventory;
        AmmoNeedTakeFromInv = AvaiableAmmoInInventory;
    }
    else
    {
        AdditionalWeaponInfo.Round += NeedToReload;
        AmmoNeedTakeFromInv = NeedToReload;
    }

    OnWeaponReloadEnd.Broadcast(true, -AmmoNeedTakeFromInv);
}

void AWeaponBase::CancelReload()
{
    bWeaponReloading = false;
    OnWeaponReloadEnd.Broadcast(false, 0);
    bDropClip = false;
}

bool AWeaponBase::CheckCanWeaponReload()
{
    bool bResult = true;
    if (GetOwner())
    {
        const auto Inventory = Cast<UInventoryComponent>(GetOwner()->GetComponentByClass(UInventoryComponent::StaticClass()));
        int32 AviableAmmoForWeapon;
        if (!Inventory->CheckAmmoForWeapon(WeaponSettings.WeaponType, AviableAmmoForWeapon))
        {
            bResult = false;
            Inventory->OnWeaponNotHaveRound.Broadcast(Inventory->GetWeaponIndexSlotByName(WeaponID_Name));
            Inventory->WeaponNotHaveRoundEvent_Multicast(Inventory->GetWeaponIndexSlotByName(WeaponID_Name));
        }
        else
        {
            // Inventory->OnWeaponHaveRound.Broadcast(Inventory->GetWeaponIndexSlotByName(WeaponID_Name));
            Inventory->WeaponHaveRoundEvent_Multicast(Inventory->GetWeaponIndexSlotByName(WeaponID_Name));
        }
    }

    return bResult;
}

int32 AWeaponBase::GetAviableAmmoForReload()
{
    int32 AviableAmmoForWeapon = WeaponSettings.ClipSize;
    if (!GetOwner()) return AviableAmmoForWeapon;

    const auto InventoryComponent = Cast<UInventoryComponent>(GetOwner()->GetComponentByClass(UInventoryComponent::StaticClass()));
    if (!InventoryComponent) return AviableAmmoForWeapon;

    if (InventoryComponent->CheckAmmoForWeapon(WeaponSettings.WeaponType, AviableAmmoForWeapon))
    {
    }

    return AviableAmmoForWeapon;
}

void AWeaponBase::PlayReloadFX()
{
    UAnimMontage* CharAnimToPlay = nullptr;
    UAnimMontage* WeaponAnimToPlay = nullptr;

    if (bAiming)
    {
        CharAnimToPlay = WeaponSettings.CharacterAnimation.CharacterReloadIronsights;
        WeaponAnimToPlay = WeaponSettings.WeaponAnimation.WeaponReloadIronsights;
    }
    else
    {
        CharAnimToPlay = WeaponSettings.CharacterAnimation.CharacterReloadHip;
        WeaponAnimToPlay = WeaponSettings.WeaponAnimation.WeaponReloadHip;
    }

    UGameplayStatics::PlaySoundAtLocation(GetWorld(), WeaponSettings.SoundReloadWeapon, GetActorLocation());

    AnimWeaponStart_Multicast(WeaponAnimToPlay);
    OnWeaponReloadStart.Broadcast(CharAnimToPlay);
}

void AWeaponBase::InitDropMesh(FDropMeshData DropMeshInfo, UArrowComponent* DropTransform)
{
    if (!DropMeshInfo.DropItemClass) return;

    FTransform SpawnTransform;
    SpawnTransform.SetLocation(DropTransform->GetComponentLocation());
    SpawnTransform.SetRotation(FQuat(DropTransform->GetComponentRotation()));

    auto NewDropMesh = GetWorld()->SpawnActorDeferred<ADropShell>(DropMeshInfo.DropItemClass, SpawnTransform);
    if (NewDropMesh)
    {
        NewDropMesh->SetDropImpulse(DropMeshInfo.DropImpulse);
        NewDropMesh->SetLifeSpan(DropMeshInfo.LifeTime);

        NewDropMesh->FinishSpawning(SpawnTransform);
    }
}

/** Get & Set functions */
FVector AWeaponBase::ApplyDispersionToShoot(FVector DirectionShoot) const
{
    return FMath::VRandCone(DirectionShoot, GetCurrentDispersion() * PI / 180.f);
}

float AWeaponBase::GetCurrentDispersion() const
{
    float Result = CurrentDispersion;
    return Result;
}

bool AWeaponBase::CheckWeaponCanFire() const
{
    return !bBlockFire;
}

bool AWeaponBase::GetWeaponReloading() const
{
    return bWeaponReloading;
}

bool AWeaponBase::CanWeaponReload()
{
    return true;
}

int32 AWeaponBase::GetWeaponRound() const
{
    return AdditionalWeaponInfo.Round;
}

int8 AWeaponBase::GetNumberProjectileByShot() const
{
    return WeaponSettings.NumberBulletsByShot;
}

void AWeaponBase::UpdateWeaponByCharacterMovementState_OnServer_Implementation(FVector NewShootEndLocation, bool NewShootReduceDispersion)
{
    ShootEndLocation = NewShootEndLocation;
}

void AWeaponBase::AnimWeaponStart_Multicast_Implementation(UAnimMontage* Anim)
{
    if (SkeletalMeshWeapon && Anim)
    {
        SkeletalMeshWeapon->PlayAnimation(Anim, false);
    }
}

void AWeaponBase::ShellDropFire_Multicast_Implementation() {}

void AWeaponBase::WeaponFireFX_Multicast_Implementation(UParticleSystem* FireFX, USoundCue* Sound)
{
    if (FireFX)
    {
        UGameplayStatics::SpawnEmitterAttached(FireFX, ShootLocation);
    }

    if (Sound)
    {
        UGameplayStatics::SpawnSoundAtLocation(GetWorld(), Sound, ShootLocation->GetComponentLocation());
    }
}

void AWeaponBase::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);

    DOREPLIFETIME(AWeaponBase, AdditionalWeaponInfo);
    DOREPLIFETIME(AWeaponBase, bWeaponReloading);
    DOREPLIFETIME(AWeaponBase, ShootEndLocation);
}