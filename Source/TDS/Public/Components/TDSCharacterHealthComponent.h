// Copyright Elin Denis. All Rights Reserved.
// Developed with Unreal Engine 5.0
#pragma once

#include "CoreMinimal.h"
#include "Components/TDSHealthComponent.h"
#include "TDSCharacterHealthComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnShieldChangeSignature, float, Shield, float, Damage);

UCLASS()
class TDS_API UTDSCharacterHealthComponent : public UTDSHealthComponent
{
    GENERATED_BODY()

public:
    UPROPERTY(BlueprintAssignable, EditDefaultsOnly, BlueprintReadWrite, Category = "Delegates")
    FOnShieldChangeSignature OnShieldChange;

protected:
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Shield")
    float CoolDownShieldRecoveryTime = 5.0f;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Shield")
    float ShieldRecoveryValue = 1.0f;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Shield")
    float ShieldRecoveryRate = 0.1f;

private:
    UPROPERTY(Replicated)
    float Shield = 100.0f;

    FTimerHandle CoolDownShieldTimer;
    FTimerHandle ShieldRecoveryRateTimer;

public:
    virtual void ChangeHealthValue_OnServer(float Value) override;

    void ChangeShieldValue(float Value);

    UFUNCTION(BlueprintCallable)
    float GetShieldValue() const;

    UFUNCTION(NetMulticast, Reliable)
    void ShieldChangeEvent_Multicast(float NewShield, float Damage);

private:
    void CoolDownShieldEnd();

    void RecoveryShield();
};
