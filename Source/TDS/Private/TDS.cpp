// Copyright Elin Denis. All Rights Reserved.
// Developed with Unreal Engine 5.0
#include "TDS.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE(FDefaultGameModuleImpl, TDS, "TDS");

DEFINE_LOG_CATEGORY(LogTDS)
DEFINE_LOG_CATEGORY(LogTDS_Net)