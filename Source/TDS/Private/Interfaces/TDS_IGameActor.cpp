// Copyright Elin Denis. All Rights Reserved.
// Developed with Unreal Engine 5.0
#include "Interfaces/TDS_IGameActor.h"

// Project class include
#include "StateEffects/TDS_StateEffect.h"

EPhysicalSurface ITDS_IGameActor::GetSurfaceType()
{
    return EPhysicalSurface::SurfaceType_Default;
}

TArray<UTDS_StateEffect*> ITDS_IGameActor::GetAllCurrentEffects() const
{
    TArray<UTDS_StateEffect*> Effects;
    return Effects;
}