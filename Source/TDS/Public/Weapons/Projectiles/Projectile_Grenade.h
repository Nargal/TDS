// Copyright Elin Denis. All Rights Reserved.
// Developed with Unreal Engine 5.0
#pragma once

#include "CoreMinimal.h"
#include "Weapons/Projectiles/Projectile.h"
#include "Projectile_Grenade.generated.h"

UCLASS()
class TDS_API AProjectile_Grenade : public AProjectile
{
    GENERATED_BODY()

public:
    bool TimerEnabled = false;
    float TimerToExplose = 0.0f;
    float TimeToExplose = 5.0f;

protected:
    virtual void BeginPlay() override;
    virtual void EndPlay(const EEndPlayReason::Type EndPlayReason);

public:
    virtual void OnProjectileHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit) override;

    virtual void ImpactProjectile() override;

    void Explosion();
};
