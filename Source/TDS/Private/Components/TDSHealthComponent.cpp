// Copyright Elin Denis. All Rights Reserved.
// Developed with Unreal Engine 5.0
#include "Components/TDSHealthComponent.h"

// Engine class include
#include "Net/UnrealNetwork.h"

UTDSHealthComponent::UTDSHealthComponent()
{
    PrimaryComponentTick.bCanEverTick = true;
    SetIsReplicatedByDefault(true);
}

void UTDSHealthComponent::BeginPlay()
{
    Super::BeginPlay();
}

void UTDSHealthComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
    Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

float UTDSHealthComponent::GetCurrentHealth() const
{
    return Health;
}

void UTDSHealthComponent::SetCurrentHealth(float NewHealth)
{
    Health = NewHealth;
}

void UTDSHealthComponent::ChangeHealthValue_OnServer_Implementation(float Value)
{
    if (bIsAlive)
    {
        Value = Value * CoefDamage;
        Health += Value;

        HealthChangeEvent_Multicast(Health, Value);

        if (Health > 100.0f)
        {
            Health = 100.0f;
        }
        else
        {
            if (Health <= 0.0f)
            {
                bIsAlive = false;
                DeadEvent_Multicast();
            }
        }
    }
}

void UTDSHealthComponent::HealthChangeEvent_Multicast_Implementation(float NewHealth, float NewValue)
{
    OnHealthChange.Broadcast(NewHealth, NewValue);
}

void UTDSHealthComponent::DeadEvent_Multicast_Implementation()
{
    OnDead.Broadcast();
}

bool UTDSHealthComponent::GetIsAlive() const
{
    return bIsAlive;
}

void UTDSHealthComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);

    DOREPLIFETIME(UTDSHealthComponent, Health);
    DOREPLIFETIME(UTDSHealthComponent, bIsAlive);
}