// Copyright Elin Denis. All Rights Reserved.
// Developed with Unreal Engine 5.0
#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "TDSHealthComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnHealthChangeSignature, float, Health, float, Damage);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDeadSignature);

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class TDS_API UTDSHealthComponent : public UActorComponent
{
    GENERATED_BODY()

public:
    UTDSHealthComponent();

    UPROPERTY(BlueprintAssignable, EditDefaultsOnly, BlueprintReadWrite, Category = "Delegates")
    FOnHealthChangeSignature OnHealthChange;

    UPROPERTY(BlueprintAssignable, EditDefaultsOnly, BlueprintReadWrite, Category = "Delegates")
    FOnDeadSignature OnDead;

protected:
    UPROPERTY(Replicated)
    float Health = 100.0f;

    UPROPERTY(Replicated)
    bool bIsAlive = true;

    virtual void BeginPlay() override;

public:
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Health")
    float CoefDamage = 1.0f;

    virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

    UFUNCTION(BlueprintCallable, Category = "Health")
    float GetCurrentHealth() const;

    UFUNCTION(BlueprintCallable, Category = "Health")
    bool GetIsAlive() const;

    UFUNCTION(BlueprintCallable, Category = "Health")
    void SetCurrentHealth(float NewHealth);

    UFUNCTION(Server, Reliable, BlueprintCallable, Category = "Health")
    virtual void ChangeHealthValue_OnServer(float Value);

    UFUNCTION(NetMulticast, Reliable)
    void HealthChangeEvent_Multicast(float NewHealth, float NewValue);

    UFUNCTION(NetMulticast, Reliable)
    void DeadEvent_Multicast();
};
