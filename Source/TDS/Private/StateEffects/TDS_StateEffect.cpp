// Copyright Elin Denis. All Rights Reserved.
// Developed with Unreal Engine 5.0
#include "StateEffects/TDS_StateEffect.h"

// Engine class include
#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystemComponent.h"
#include "Net/UnrealNetwork.h"

// Project class include
#include "Components/TDSHealthComponent.h"
#include "Interfaces/TDS_IGameActor.h"

DEFINE_LOG_CATEGORY_STATIC(LogTDS_StateEffect, All, All);

bool UTDS_StateEffect::InitObject(AActor* Actor, FName NameBoneHit)
{
    MyActor = Actor;
    NameBone = NameBoneHit;

    ITDS_IGameActor* Interface = Cast<ITDS_IGameActor>(MyActor);
    if (Interface)
    {
        Interface->Execute_AddEffect(MyActor, this);
    }

    return true;
}

void UTDS_StateEffect::DestroyObject()
{
    ITDS_IGameActor* Interface = Cast<ITDS_IGameActor>(MyActor);
    if (Interface)
    {
        Interface->Execute_RemoveEffect(MyActor, this);
    }

    MyActor = nullptr;
    if (this && this->IsValidLowLevel())
    {
        this->ConditionalBeginDestroy();
    }
}

bool UTDS_StateEffect_ExecuteOnce::InitObject(AActor* Actor, FName NameBoneHit)
{
    Super::InitObject(Actor, NameBoneHit);
    ExecuteOnce();
    return true;
}

void UTDS_StateEffect_ExecuteOnce::DestroyObject()
{
    Super::DestroyObject();
}

void UTDS_StateEffect_ExecuteOnce::ExecuteOnce()
{
    if (MyActor)
    {
        auto HealthComponent = Cast<UTDSHealthComponent>(MyActor->GetComponentByClass(UTDSHealthComponent::StaticClass()));
        if (HealthComponent)
        {
            HealthComponent->ChangeHealthValue_OnServer(Power);
        }
    }

    DestroyObject();
}

bool UTDS_StateEffect_ExecuteTimer::InitObject(AActor* Actor, FName NameBoneHit)
{
    Super::InitObject(Actor, NameBoneHit);

    if (GetWorld())
    {
        GetWorld()->GetTimerManager().SetTimer(ExicuteTimerHandle, this, &UTDS_StateEffect_ExecuteTimer::DestroyObject, Timer, false);
        GetWorld()->GetTimerManager().SetTimer(EffectTimerHandle, this, &UTDS_StateEffect_ExecuteTimer::Execute, RateTime, true);
    }

    return true;
}

void UTDS_StateEffect_ExecuteTimer::DestroyObject()
{
    if (GetWorld())
    {
        GetWorld()->GetTimerManager().ClearAllTimersForObject(this);
    }

    Super::DestroyObject();
}

void UTDS_StateEffect_ExecuteTimer::Execute()
{
    if (MyActor)
    {
        auto HealthComponent = Cast<UTDSHealthComponent>(MyActor->GetComponentByClass(UTDSHealthComponent::StaticClass()));
        if (HealthComponent)
        {
            HealthComponent->ChangeHealthValue_OnServer(Power);
        }
    }
}

void UTDS_StateEffect::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);

    DOREPLIFETIME(UTDS_StateEffect, NameBone);
}