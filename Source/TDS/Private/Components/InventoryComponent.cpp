// Copyright Elin Denis. All Rights Reserved.
// Developed with Unreal Engine 5.0
#include "Components/InventoryComponent.h"

// Engine class include
#include "Net/UnrealNetwork.h"

// Project class include
#include "Game/TDSGameInstance.h"
#include "Interfaces/TDS_IGameActor.h"

DEFINE_LOG_CATEGORY_STATIC(LogInventoryComponent, All, All);

UInventoryComponent::UInventoryComponent()
{
    PrimaryComponentTick.bCanEverTick = false;
    SetIsReplicatedByDefault(true);
}

void UInventoryComponent::BeginPlay()
{
    Super::BeginPlay();
}

bool UInventoryComponent::SwitchWeaponToIndex(int32 ChangeToIndex, int32 OldIndex, FAdditionalWeaponData OldInfo, bool bIsForward)
{
    bool bIsSuccess = false;
    int8 CorrectIndex = ChangeToIndex;
    if (ChangeToIndex > WeaponSlots.Num() - 1)
        CorrectIndex = 0;
    else if (ChangeToIndex < 0)
        CorrectIndex = WeaponSlots.Num() - 1;

    FName NewIdWeapon;
    FAdditionalWeaponData NewAdditionalInfo;
    int32 NewCurrentIndex = 0;

    if (WeaponSlots.IsValidIndex(CorrectIndex))
    {
        if (!WeaponSlots[CorrectIndex].WeaponID_Name.IsNone())
        {
            if (WeaponSlots[CorrectIndex].AdditionalInfo.Round > 0)
                bIsSuccess = true;
            else
            {
                UTDSGameInstance* myGI = Cast<UTDSGameInstance>(GetWorld()->GetGameInstance());
                if (myGI)
                {
                    // check ammoSlots for this weapon
                    FWeaponData myInfo;
                    myGI->GetWeaponInfoByName(WeaponSlots[CorrectIndex].WeaponID_Name, myInfo);

                    bool bIsFind = false;
                    int8 j = 0;
                    while (j < AmmoSlots.Num() && !bIsFind)
                    {
                        if (AmmoSlots[j].WeaponType == myInfo.WeaponType && AmmoSlots[j].Bullets > 0)
                        {
                            // good weapon have ammo start change
                            bIsSuccess = true;
                            bIsFind = true;
                        }
                        j++;
                    }
                }
            }
            if (bIsSuccess)
            {
                NewCurrentIndex = CorrectIndex;
                NewIdWeapon = WeaponSlots[CorrectIndex].WeaponID_Name;
                NewAdditionalInfo = WeaponSlots[CorrectIndex].AdditionalInfo;
            }
        }
    }

    if (!bIsSuccess)
    {
        int8 iteration = 0;
        int8 Seconditeration = 0;
        int8 tmpIndex = 0;
        while (iteration < WeaponSlots.Num() && !bIsSuccess)
        {
            iteration++;

            if (bIsForward)
            {
                // Seconditeration = 0;

                tmpIndex = ChangeToIndex + iteration;
            }
            else
            {
                Seconditeration = WeaponSlots.Num() - 1;

                tmpIndex = ChangeToIndex - iteration;
            }

            if (WeaponSlots.IsValidIndex(tmpIndex))
            {
                if (!WeaponSlots[tmpIndex].WeaponID_Name.IsNone())
                {
                    if (WeaponSlots[tmpIndex].AdditionalInfo.Round > 0)
                    {
                        // WeaponGood
                        bIsSuccess = true;
                        NewIdWeapon = WeaponSlots[tmpIndex].WeaponID_Name;
                        NewAdditionalInfo = WeaponSlots[tmpIndex].AdditionalInfo;
                        NewCurrentIndex = tmpIndex;
                    }
                    else
                    {
                        FWeaponData myInfo;
                        UTDSGameInstance* myGI = Cast<UTDSGameInstance>(GetWorld()->GetGameInstance());

                        myGI->GetWeaponInfoByName(WeaponSlots[tmpIndex].WeaponID_Name, myInfo);

                        bool bIsFind = false;
                        int8 j = 0;
                        while (j < AmmoSlots.Num() && !bIsFind)
                        {
                            if (AmmoSlots[j].WeaponType == myInfo.WeaponType && AmmoSlots[j].Bullets > 0)
                            {
                                // WeaponGood
                                bIsSuccess = true;
                                NewIdWeapon = WeaponSlots[tmpIndex].WeaponID_Name;
                                NewAdditionalInfo = WeaponSlots[tmpIndex].AdditionalInfo;
                                NewCurrentIndex = tmpIndex;
                                bIsFind = true;
                            }
                            j++;
                        }
                    }
                }
            }
            else
            {
                // go to end of LEFT of array weapon slots
                if (OldIndex != Seconditeration)
                {
                    if (WeaponSlots.IsValidIndex(Seconditeration))
                    {
                        if (!WeaponSlots[Seconditeration].WeaponID_Name.IsNone())
                        {
                            if (WeaponSlots[Seconditeration].AdditionalInfo.Round > 0)
                            {
                                // WeaponGood
                                bIsSuccess = true;
                                NewIdWeapon = WeaponSlots[Seconditeration].WeaponID_Name;
                                NewAdditionalInfo = WeaponSlots[Seconditeration].AdditionalInfo;
                                NewCurrentIndex = Seconditeration;
                            }
                            else
                            {
                                FWeaponData myInfo;
                                UTDSGameInstance* myGI = Cast<UTDSGameInstance>(GetWorld()->GetGameInstance());

                                myGI->GetWeaponInfoByName(WeaponSlots[Seconditeration].WeaponID_Name, myInfo);

                                bool bIsFind = false;
                                int8 j = 0;
                                while (j < AmmoSlots.Num() && !bIsFind)
                                {
                                    if (AmmoSlots[j].WeaponType == myInfo.WeaponType && AmmoSlots[j].Bullets > 0)
                                    {
                                        // WeaponGood
                                        bIsSuccess = true;
                                        NewIdWeapon = WeaponSlots[Seconditeration].WeaponID_Name;
                                        NewAdditionalInfo = WeaponSlots[Seconditeration].AdditionalInfo;
                                        NewCurrentIndex = Seconditeration;
                                        bIsFind = true;
                                    }
                                    j++;
                                }
                            }
                        }
                    }
                }
                else
                {
                    // go to same weapon when start
                    if (WeaponSlots.IsValidIndex(Seconditeration))
                    {
                        if (!WeaponSlots[Seconditeration].WeaponID_Name.IsNone())
                        {
                            if (WeaponSlots[Seconditeration].AdditionalInfo.Round > 0)
                            {
                                // WeaponGood, it same weapon do nothing
                            }
                            else
                            {
                                FWeaponData myInfo;
                                UTDSGameInstance* myGI = Cast<UTDSGameInstance>(GetWorld()->GetGameInstance());

                                myGI->GetWeaponInfoByName(WeaponSlots[Seconditeration].WeaponID_Name, myInfo);

                                bool bIsFind = false;
                                int8 j = 0;
                                while (j < AmmoSlots.Num() && !bIsFind)
                                {
                                    if (AmmoSlots[j].WeaponType == myInfo.WeaponType)
                                    {
                                        if (AmmoSlots[j].Bullets > 0)
                                        {
                                            // WeaponGood, it same weapon do nothing
                                        }
                                        else
                                        {
                                            // Not find weapon with ammo need init Pistol with infinity ammo
                                            UE_LOG(LogTemp, Error, TEXT("UTPSInventoryComponent::SwitchWeaponToIndex - Init PISTOL - NEED"));
                                        }
                                    }
                                    j++;
                                }
                            }
                        }
                    }
                }
                if (bIsForward)
                {
                    Seconditeration++;
                }
                else
                {
                    Seconditeration--;
                }
            }
        }
    }
    if (bIsSuccess)
    {
        SetAdditionalInfoWeapon(OldIndex, OldInfo);
        SwitchWeaponEvent_OnServer(NewIdWeapon, NewAdditionalInfo, NewCurrentIndex);
        // OnSwitchWeapon.Broadcast(NewIdWeapon, NewAdditionalInfo, NewCurrentIndex);
    }

    return bIsSuccess;
}

bool UInventoryComponent::SwitchWeaponByIndex(int32 IndexWeaponToChange, int32 PreviosIndex, FAdditionalWeaponData PreviosWeaponInfo)
{
    bool bIsSuccess = false;
    FName ToSwitchIdWeapon;
    FAdditionalWeaponData ToSwitchAdditionalInfo;

    ToSwitchIdWeapon = GetWeaponNameBySlotIndex(IndexWeaponToChange);
    ToSwitchAdditionalInfo = GetAdditionalInfoWeapon(IndexWeaponToChange);

    if (!ToSwitchIdWeapon.IsNone())
    {
        SetAdditionalInfoWeapon(PreviosIndex, PreviosWeaponInfo);
        SwitchWeaponEvent_OnServer(ToSwitchIdWeapon, ToSwitchAdditionalInfo, IndexWeaponToChange);
        // OnSwitchWeapon.Broadcast(ToSwitchIdWeapon, ToSwitchAdditionalInfo, IndexWeaponToChange);

        // check ammo slot for event to player
        EWeaponType ToSwitchWeaponType;
        if (GetWeaponTypeByNameWeapon(ToSwitchIdWeapon, ToSwitchWeaponType))
        {
            int32 AviableAmmoForWeapon = -1;
            if (CheckAmmoForWeapon(ToSwitchWeaponType, AviableAmmoForWeapon))
            {
            }
        }

        bIsSuccess = true;
    }

    return bIsSuccess;
}

FName UInventoryComponent::GetWeaponNameBySlotIndex(int32 IndexSlot) const
{
    FName result;
    if (WeaponSlots.IsValidIndex(IndexSlot))
    {
        result = WeaponSlots[IndexSlot].WeaponID_Name;
    }
    return result;
}

bool UInventoryComponent::GetWeaponTypeByIndexSlot(int32 IndexSlot, EWeaponType& WeaponType)
{
    bool bIsFind = false;
    FWeaponData OutInfo;
    WeaponType = EWeaponType::RIFLE_TYPE;
    UTDSGameInstance* GI = Cast<UTDSGameInstance>(GetWorld()->GetGameInstance());
    if (GI)
    {
        if (WeaponSlots.IsValidIndex(IndexSlot))
        {
            GI->GetWeaponInfoByName(WeaponSlots[IndexSlot].WeaponID_Name, OutInfo);
            WeaponType = OutInfo.WeaponType;
            bIsFind = true;
        }
    }
    return bIsFind;
}

bool UInventoryComponent::GetWeaponTypeByNameWeapon(FName WeaponID_Name, EWeaponType& WeaponType)
{
    bool bIsFind = false;
    FWeaponData OutInfo;
    WeaponType = EWeaponType::RIFLE_TYPE;
    UTDSGameInstance* GI = Cast<UTDSGameInstance>(GetWorld()->GetGameInstance());
    if (GI)
    {
        GI->GetWeaponInfoByName(WeaponID_Name, OutInfo);
        WeaponType = OutInfo.WeaponType;
        bIsFind = true;
    }
    return bIsFind;
}

FAdditionalWeaponData UInventoryComponent::GetAdditionalInfoWeapon(int32 IndexWeapon)
{
    FAdditionalWeaponData Result;
    if (!WeaponSlots.IsValidIndex(IndexWeapon))
    {
        bool bIsFind = false;
        int8 i = 0;
        while (i < WeaponSlots.Num() && !bIsFind)
        {
            if (i == IndexWeapon)
            {
                Result = WeaponSlots[i].AdditionalInfo;
                bIsFind = true;
            }
            i++;
        }
        if (!bIsFind) UE_LOG(LogInventoryComponent, Warning, TEXT("UInventoryComponent::GetAdditionalInfoWeapon - No Found Weapon with index - %d"), IndexWeapon);
    }
    else
        UE_LOG(LogInventoryComponent, Warning, TEXT("UInventoryComponent::GetAdditionalInfoWeapon - Not Correct index Weapon - %d"), IndexWeapon);

    return Result;
}

int32 UInventoryComponent::GetWeaponIndexSlotByName(FName WeaponID_Name)
{
    for (int i = 0; i < WeaponSlots.Num(); i++)
    {
        if (WeaponSlots[i].WeaponID_Name == WeaponID_Name) return i;
    }

    return -1;
}

void UInventoryComponent::SetAdditionalInfoWeapon(int32 IndexWeapon, FAdditionalWeaponData NewInfo)
{
    if (!WeaponSlots.IsValidIndex(IndexWeapon)) return;

    int8 i = 0;
    bool bIsFind = false;
    while (i < WeaponSlots.Num() && !bIsFind)
    {
        if (i == IndexWeapon)
        {
            WeaponSlots[i].AdditionalInfo = NewInfo;
            bIsFind = true;

            WeaponAdditionalInfoChangeEvent_Multicast(IndexWeapon, NewInfo);
        }
        else
        {
            UE_LOG(LogInventoryComponent, Warning, TEXT("UInventoryComponent::SetAAdditionalInfoWeapon -> Not found weapon with index: %d"), IndexWeapon);
        }

        i++;
    }
}

void UInventoryComponent::AmmoSlotChangeValue(EWeaponType WeaponType, int32 CoutChangeAmmo)
{
    bool bIsFind = false;
    int8 i = 0;
    while (i < AmmoSlots.Num() && !bIsFind)
    {
        if (AmmoSlots[i].WeaponType == WeaponType)
        {
            AmmoSlots[i].Bullets += CoutChangeAmmo;
            if (AmmoSlots[i].Bullets > AmmoSlots[i].MaxBullets) AmmoSlots[i].Bullets = AmmoSlots[i].MaxBullets;

            AmmoChangeEvent_Multicast(AmmoSlots[i].WeaponType, AmmoSlots[i].Bullets);
            bIsFind = true;
        }

        i++;
    }
}

bool UInventoryComponent::CheckAmmoForWeapon(EWeaponType WeaponType, int32& AviableAmmoForWeapon)
{
    AviableAmmoForWeapon = 0;
    bool bIsFind = false;
    int8 i = 0;
    while (i < AmmoSlots.Num() && !bIsFind)
    {
        if (AmmoSlots[i].WeaponType == WeaponType)
        {
            bIsFind = true;
            AviableAmmoForWeapon = AmmoSlots[i].Bullets;
            if (AmmoSlots[i].Bullets > 0)
            {
                return true;
            }
        }
        i++;
    }

    if (AviableAmmoForWeapon <= 0)
    {
        WeaponAmmoEmptyEvent_Multicast(WeaponType);
    }
    else
    {
        WeaponAmmoAviableEvent_Multicast(WeaponType);
    }

    return false;
}

bool UInventoryComponent::CheckCanTakeAmmo(EWeaponType AmmoType)
{
    bool Result = false;
    int8 i = 0;
    while (i < AmmoSlots.Num() && !Result)
    {
        if (AmmoSlots[i].WeaponType == AmmoType && AmmoSlots[i].Bullets < AmmoSlots[i].MaxBullets) Result = true;

        i++;
    }

    return Result;
}

bool UInventoryComponent::CheckCanTakeWeapon(int32& FreeSlot)
{
    bool bIsFreeSlot = false;
    int8 i = 0;
    while (i < WeaponSlots.Num() && !bIsFreeSlot)
    {
        if (WeaponSlots[i].WeaponID_Name.IsNone())
        {
            bIsFreeSlot = true;
            FreeSlot = i;
        }
        i++;
    }

    return bIsFreeSlot;
}

bool UInventoryComponent::SwitchWeaponToInventory(FWeaponSlotData NewWeapon, int32 IndexSlot, int32 CurrentIndexWeaponChar, FDropItemData& DropItemInfo)
{
    bool Result = false;
    if (WeaponSlots.IsValidIndex(IndexSlot) && GetDropItemInfoFromInventory(IndexSlot, DropItemInfo))
    {
        WeaponSlots[IndexSlot] = NewWeapon;
        SwitchWeaponToIndex(CurrentIndexWeaponChar, -1, NewWeapon.AdditionalInfo, true);
        UpdateWeaponSlotsEvent_Multicast(IndexSlot, NewWeapon);
        Result = true;
    }

    return Result;
}

void UInventoryComponent::TryGetWeaponToInventory_OnServer_Implementation(AActor* PickupActor, FWeaponSlotData NewWeapon)
{
    int32 IndexSlot = -1;
    if (CheckCanTakeWeapon(IndexSlot))
    {
        if (WeaponSlots.IsValidIndex(IndexSlot))
        {
            WeaponSlots[IndexSlot] = NewWeapon;
            UpdateWeaponSlotsEvent_Multicast(IndexSlot, NewWeapon);

            if (PickupActor)
            {
                PickupActor->Destroy();
            }
        }
    }
}

void UInventoryComponent::DropWeaponByIndex_OnServer_Implementation(int32 ByIndex)
{
    FDropItemData DropItemData;
    FWeaponSlotData EmtyWeaponSlotData;

    bool bIsCanDrop = false;
    int8 i = 0;
    int8 AviableWeaponNum = 0;
    while (i < WeaponSlots.Num() && !bIsCanDrop)
    {
        if (!WeaponSlots[i].WeaponID_Name.IsNone())
        {
            AviableWeaponNum++;
            if (AviableWeaponNum > 1) bIsCanDrop = true;
        }
        i++;
    }

    if (bIsCanDrop && WeaponSlots.IsValidIndex(ByIndex) && GetDropItemInfoFromInventory(ByIndex, DropItemData))
    {
        bool bIsFindWeapon = false;
        int8 j = 0;
        while (j < WeaponSlots.Num() && !bIsFindWeapon)
        {
            if (!WeaponSlots[j].WeaponID_Name.IsNone())
            {
                bIsFindWeapon = true;
                SwitchWeaponEvent_OnServer(WeaponSlots[j].WeaponID_Name, WeaponSlots[j].AdditionalInfo, j);
            }
            j++;
        }

        WeaponSlots[ByIndex] = EmtyWeaponSlotData;
        if (GetOwner()->GetClass()->ImplementsInterface(UTDS_IGameActor::StaticClass()))
        {
            ITDS_IGameActor::Execute_DropWeaponToWorld(GetOwner(), DropItemData);
        }

        UpdateWeaponSlotsEvent_Multicast(ByIndex, EmtyWeaponSlotData);
    }
}

bool UInventoryComponent::GetDropItemInfoFromInventory(int32 IndexSlot, FDropItemData& DropItemInfo)
{
    bool Result = false;
    if (!GetWorld()) return Result;

    const FName DropItemName = GetWeaponNameBySlotIndex(IndexSlot);
    const auto GameInstance = Cast<UTDSGameInstance>(GetWorld()->GetGameInstance());
    if (!GameInstance) return Result;

    Result = GameInstance->GetDropItemInfoByWeaponName(DropItemName, DropItemInfo);
    if (WeaponSlots.IsValidIndex(IndexSlot))
    {
        DropItemInfo.WeaponInfo.AdditionalInfo = WeaponSlots[IndexSlot].AdditionalInfo;
    }

    return Result;
}

TArray<FWeaponSlotData> UInventoryComponent::GetWeaponSlotsData() const
{
    return WeaponSlots;
}

TArray<FAmmoSlotData> UInventoryComponent::GetAmmoSlotsData() const
{
    return AmmoSlots;
}

void UInventoryComponent::InitInventory_OnServer_Implementation(const TArray<FWeaponSlotData>& NewWeaponSlotsData, const TArray<FAmmoSlotData>& NewAmmoSlotsData)
{
    WeaponSlots = NewWeaponSlotsData;
    AmmoSlots = NewAmmoSlotsData;

    MaxSlotWeapon = WeaponSlots.Num();
    if (WeaponSlots.IsValidIndex(0))
    {
        if (!WeaponSlots[0].WeaponID_Name.IsNone())
        {
            SwitchWeaponEvent_OnServer(WeaponSlots[0].WeaponID_Name, WeaponSlots[0].AdditionalInfo, 0);
        }
    }
}

void UInventoryComponent::AmmoChangeEvent_Multicast_Implementation(EWeaponType WeaponType, int32 Cout)
{
    OnAmmoChange.Broadcast(WeaponType, Cout);
}

void UInventoryComponent::SwitchWeaponEvent_OnServer_Implementation(FName WeaponName, FAdditionalWeaponData AdditionalWeaponData, int32 IndexSlot)
{
    OnSwitchWeapon.Broadcast(WeaponName, AdditionalWeaponData, IndexSlot);
}

void UInventoryComponent::WeaponAdditionalInfoChangeEvent_Multicast_Implementation(int32 IndexSlot, FAdditionalWeaponData AdditionalWeaponData)
{
    OnWeaponAdditionalInfo.Broadcast(IndexSlot, AdditionalWeaponData);
}

void UInventoryComponent::WeaponAmmoAviableEvent_Multicast_Implementation(EWeaponType WeaponType)
{
    OnWeaponAmmoAviable.Broadcast(WeaponType);
}

void UInventoryComponent::WeaponAmmoEmptyEvent_Multicast_Implementation(EWeaponType WeaponType)
{
    OnWeaponAmmoEmpty.Broadcast(WeaponType);
}

void UInventoryComponent::UpdateWeaponSlotsEvent_Multicast_Implementation(int32 IndexSlotChange, FWeaponSlotData WeaponSlotData)
{
    OnUpdateWeaponSlots.Broadcast(IndexSlotChange, WeaponSlotData);
}

void UInventoryComponent::WeaponNotHaveRoundEvent_Multicast_Implementation(int32 IndexSlotWeapon)
{
    OnWeaponNotHaveRound.Broadcast(IndexSlotWeapon);
}

void UInventoryComponent::WeaponHaveRoundEvent_Multicast_Implementation(int32 IndexSlotWeapon)
{
    OnWeaponHaveRound.Broadcast(IndexSlotWeapon);
}

void UInventoryComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);

    DOREPLIFETIME(UInventoryComponent, WeaponSlots);
    DOREPLIFETIME(UInventoryComponent, AmmoSlots);
}