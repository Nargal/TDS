// Copyright Elin Denis. All Rights Reserved.
// Developed with Unreal Engine 5.0
using UnrealBuildTool;
using System.Collections.Generic;

public class TDSTarget : TargetRules
{
    public TDSTarget(TargetInfo Target) : base(Target)
    {
        Type = TargetType.Game;
        DefaultBuildSettings = BuildSettingsVersion.V2;
        ExtraModuleNames.Add("TDS");
    }
}