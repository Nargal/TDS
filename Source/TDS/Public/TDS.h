// Copyright Elin Denis. All Rights Reserved.
// Developed with Unreal Engine 5.0
#pragma once

#include "CoreMinimal.h"

DECLARE_LOG_CATEGORY_EXTERN(LogTDS, Log, All);
DECLARE_LOG_CATEGORY_EXTERN(LogTDS_Net, Log, All);