// Copyright Elin Denis. All Rights Reserved.
// Developed with Unreal Engine 5.0
#include "Components/TDSCharacterHealthComponent.h"

// Engine class include
#include "TimerManager.h"
#include "Net/UnrealNetwork.h"

DEFINE_LOG_CATEGORY_STATIC(LogCharacterHealthComponent, All, All)

void UTDSCharacterHealthComponent::ChangeHealthValue_OnServer(float Value)
{
    float CurrentValue = Value * CoefDamage;

    if (Shield > 0.0f && Value < 0.0f)
    {
        ChangeShieldValue(Value);
        if (FMath::IsNearlyZero(Shield))
        {
            // Spawn FX
        }
    }
    else
    {
        Super::ChangeHealthValue_OnServer(Value);
    }
}

void UTDSCharacterHealthComponent::ChangeShieldValue(float Value)
{
    Shield = FMath::Clamp(Shield + Value, 0.0f, 100.0f);
    ShieldChangeEvent_Multicast(Shield, ShieldRecoveryValue);

    if (GetWorld())
    {
        GetWorld()->GetTimerManager().SetTimer(
            CoolDownShieldTimer, this, &UTDSCharacterHealthComponent::CoolDownShieldEnd, CoolDownShieldRecoveryTime, false);
        GetWorld()->GetTimerManager().ClearTimer(ShieldRecoveryRateTimer);
    }
}

void UTDSCharacterHealthComponent::CoolDownShieldEnd()
{
    if (GetWorld())
    {
        GetWorld()->GetTimerManager().SetTimer(
            ShieldRecoveryRateTimer, this, &UTDSCharacterHealthComponent::RecoveryShield, ShieldRecoveryRate, true);
    }
}

void UTDSCharacterHealthComponent::RecoveryShield()
{
    float tmp = Shield;
    tmp = tmp + ShieldRecoveryValue;
    if (tmp > 100.0f)
    {
        Shield = 100.0f;
        if (GetWorld())
        {
            GetWorld()->GetTimerManager().ClearTimer(ShieldRecoveryRateTimer);
        }
    }
    else
    {
        Shield = tmp;
    }

    ShieldChangeEvent_Multicast(Shield, ShieldRecoveryValue);
}

float UTDSCharacterHealthComponent::GetShieldValue() const
{
    return Shield;
}

void UTDSCharacterHealthComponent::ShieldChangeEvent_Multicast_Implementation(float NewShield, float Damage)
{
    OnShieldChange.Broadcast(NewShield, Damage);
}

void UTDSCharacterHealthComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);

    DOREPLIFETIME(UTDSCharacterHealthComponent, Shield);
}