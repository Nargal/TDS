// Copyright Elin Denis. All Rights Reserved.
// Developed with Unreal Engine 5.0
#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TDSGameMode.generated.h"

UCLASS(minimalapi)
class ATDSGameMode : public AGameModeBase
{
    GENERATED_BODY()

public:
    ATDSGameMode();

    void PlayerCharacterDead();
};