// Copyright Elin Denis. All Rights Reserved.
// Developed with Unreal Engine 5.0
using UnrealBuildTool;
using System.Collections.Generic;

public class TDSEditorTarget : TargetRules
{
    public TDSEditorTarget(TargetInfo Target) : base(Target)
    {
        Type = TargetType.Editor;
        DefaultBuildSettings = BuildSettingsVersion.V2;
        ExtraModuleNames.Add("TDS");
    }
}