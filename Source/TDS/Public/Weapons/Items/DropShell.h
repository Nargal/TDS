// Copyright Elin Denis. All Rights Reserved.
// Developed with Unreal Engine 5.0
#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "DropShell.generated.h"

UCLASS()
class TDS_API ADropShell : public AActor
{
    GENERATED_BODY()

public:
    // Sets default values for this actor's properties
    ADropShell();

private:
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
    class UStaticMeshComponent* DropStaticMesh;
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
    class URotatingMovementComponent* RotMovemetComponent;
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
    class UProjectileMovementComponent* MovementComp;

    bool bRotationComponent = true;

public:
    UFUNCTION()
    void OnHitComponent(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

    void SetDropImpulse(float Value);
};
