// Copyright Elin Denis. All Rights Reserved.
// Developed with Unreal Engine 5.0
#include "Game/TDSGameMode.h"

// Engine class include
#include "UObject/ConstructorHelpers.h"

// Project class include
#include "Player/TDSPlayerController.h"
#include "Player/TDSCharacter.h"

ATDSGameMode::ATDSGameMode()
{
    DefaultPawnClass = ATDSCharacter::StaticClass();
    PlayerControllerClass = ATDSPlayerController::StaticClass();
}

void ATDSGameMode::PlayerCharacterDead() {}