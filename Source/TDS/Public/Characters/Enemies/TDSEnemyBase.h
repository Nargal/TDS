// Copyright Elin Denis. All Rights Reserved.
// Developed with Unreal Engine 5.0
#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Interfaces/TDS_IGameActor.h"
#include "TDSEnemyBase.generated.h"

class UTDS_StateEffect;
class UActorChannel;
class FOutBunch;

UCLASS()
class TDS_API ATDSEnemyBase : public ACharacter, public ITDS_IGameActor
{
    GENERATED_BODY()

public:
    ATDSEnemyBase();

    // Effects
    UPROPERTY(Replicated, EditDefaultsOnly, BlueprintReadWrite, Category = "Settings")
    TArray<UTDS_StateEffect*> Effects;

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
    void RemoveEffect(UTDS_StateEffect* RemoveEffect);
    void RemoveEffect_Implementation(UTDS_StateEffect* RemoveEffect) override;

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
    void AddEffect(UTDS_StateEffect* NewEffect);
    void AddEffect_Implementation(UTDS_StateEffect* NewEffect) override;

protected:
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Debug")
    TArray<UParticleSystemComponent*> ParticleSystemEffects;

    virtual void BeginPlay() override;

private:
    // Replicated Effects
    UPROPERTY(ReplicatedUsing = EffectAdd_OnRep)
    UTDS_StateEffect* EffectAdd = nullptr;

    UPROPERTY(ReplicatedUsing = EffectRemove_OnRep)
    UTDS_StateEffect* EffectRemove = nullptr;

    UFUNCTION()
    void EffectAdd_OnRep();

    UFUNCTION()
    void EffectRemove_OnRep();

    UFUNCTION()
    void SwitchEffect(UTDS_StateEffect* Effect, bool bIsAdd);

    UFUNCTION(Server, Reliable)
    void ExecuteEffectAdded_OnServer(UParticleSystem* ExecuteFX);

    UFUNCTION(NetMulticast, Reliable)
    void ExecuteEffectAdded_Multicast(UParticleSystem* ExecuteFX);

public:
    virtual void Tick(float DeltaTime) override;

    /** Method that allows an actor to replicate subobjects on its actor channel */
    bool ReplicateSubobjects(UActorChannel* Channel, FOutBunch* Bunch, FReplicationFlags* RepFlags) override;
};
