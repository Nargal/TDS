// Copyright Elin Denis. All Rights Reserved.
// Developed with Unreal Engine 5.0
#include "Game/TDSGameInstance.h"

// Engine class include
#include "Engine/DataTable.h"

// Project class include
#include "Weapons/WeaponBase.h"

DEFINE_LOG_CATEGORY_STATIC(LogTDSGameInstance, All, All);

bool UTDSGameInstance::GetWeaponInfoByName(FName NameWeapon, FWeaponData& OutInfo)
{
    bool bIsFind = false;
    FWeaponData* WeaponInfoRow;

    if (WeaponDataTable)
    {
        WeaponInfoRow = WeaponDataTable->FindRow<FWeaponData>(NameWeapon, "", false);
        if (WeaponInfoRow)
        {
            bIsFind = true;
            OutInfo = *WeaponInfoRow;
        }
    }
    else
    {
        UE_LOG(LogTDSGameInstance, Warning, TEXT("class UTDSGameInstance::GetWeaponInfoByName -> WeaponTable = nullptr"));
    }

    return bIsFind;
}

bool UTDSGameInstance::GetDropItemInfoByWeaponName(FName NameItem, FDropItemData& OutInfo)
{
    bool bIsFind = false;
    if (DropItemDataTable)
    {
        FDropItemData* DropItemInfoRow;
        TArray<FName> RowNames = DropItemDataTable->GetRowNames();

        int8 i = 0;
        while (i < RowNames.Num() && !bIsFind)
        {
            DropItemInfoRow = DropItemDataTable->FindRow<FDropItemData>(RowNames[i], "");
            if (DropItemInfoRow->WeaponInfo.WeaponID_Name == NameItem)
            {
                OutInfo = *DropItemInfoRow;
                bIsFind = true;
            }
            i++;
        }
    }
    else
    {
        UE_LOG(LogTDSGameInstance, Warning, TEXT("class UTDSGameInstance::GetDropItemInfoByName -> DropItemInfoTable = nullptr"));
    }

    return bIsFind;
}

bool UTDSGameInstance::GetDropItemInfoByName(FName NameItem, FDropItemData& OutInfo)
{
    bool bIsFind = false;
    FDropItemData* DropItemInfoRow;

    if (DropItemDataTable)
    {
        DropItemInfoRow = DropItemDataTable->FindRow<FDropItemData>(NameItem, "", false);
        if (DropItemInfoRow)
        {
            bIsFind = true;
            OutInfo = *DropItemInfoRow;
        }
    }

    return bIsFind;
}