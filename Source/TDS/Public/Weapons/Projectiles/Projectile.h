// Copyright Elin Denis. All Rights Reserved.
// Developed with Unreal Engine 5.0
#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TDSTypes.h"
#include "Projectile.generated.h"

class USphereComponent;
class UProjectileMovementComponent;
class UParticleSystemComponent;
class UParticleSystem;
class USoundCue;

UCLASS()
class TDS_API AProjectile : public AActor
{
    GENERATED_BODY()

public:
    // Sets default values for this actor's properties
    AProjectile();

    UFUNCTION(BlueprintCallable)
    FProjectileData GetProjectileData() const { return FireData.ProjectileData; }

protected:
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components)
    USphereComponent* SphereCollision = nullptr;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components)
    UStaticMeshComponent* BulletMesh = nullptr;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components)
    UProjectileMovementComponent* ProjectileMoveComponent = nullptr;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components)
    UParticleSystemComponent* BulletFX = nullptr;

    // Called when the game starts or when spawned
    virtual void BeginPlay() override;

    FFireData FireData;

public:
    UFUNCTION(BlueprintCallable)
    void InitProjectile(FFireData InitParams);

    UFUNCTION()
    virtual void OnProjectileHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

    UFUNCTION()
    void OnProjectileBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

    UFUNCTION()
    void OnProjectileEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

    virtual void ImpactProjectile();

    UFUNCTION(NetMulticast, Reliable)
    void SpawnHitDecal_Multicast(UMaterialInterface* Decalmaterial, FVector Size, UPrimitiveComponent* OtherComp, FHitResult Hit, float LifeTime);

    UFUNCTION(NetMulticast, Reliable)
    void SpawnHitFX_Multicast(UParticleSystem* TemplateFX, FHitResult Hit);

    UFUNCTION(NetMulticast, Reliable)
    void SpawnHitSound_Multicast(USoundCue* Sound, FHitResult Hit);

    UFUNCTION(NetMulticast, Unreliable)
    void InitVelocity_Multicast(float InitSpeed, float MaxSpeed);

    void PostNetReceiveVelocity(const FVector& NewVelocity) override;
};