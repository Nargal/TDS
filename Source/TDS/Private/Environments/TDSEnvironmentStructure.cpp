// Copyright Elin Denis. All Rights Reserved.
// Developed with Unreal Engine 5.0
#include "Environments/TDSEnvironmentStructure.h"

// Engine class include
#include "Components/StaticMeshComponent.h"
#include "Materials/MaterialInterface.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Particles/ParticleSystemComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/ActorChannel.h"
#include "Net/UnrealNetwork.h"

// Project class include
#include "StateEffects/TDS_StateEffect.h"

ATDSEnvironmentStructure::ATDSEnvironmentStructure()
{
    PrimaryActorTick.bCanEverTick = false;

    SetReplicates(true);
}

void ATDSEnvironmentStructure::BeginPlay()
{
    Super::BeginPlay();
}

EPhysicalSurface ATDSEnvironmentStructure::GetSurfaceType()
{
    const auto Mesh = Cast<UStaticMeshComponent>(GetComponentByClass(UStaticMeshComponent::StaticClass()));
    if (!Mesh) return EPhysicalSurface::SurfaceType_Default;

    const auto Material = Mesh->GetMaterial(0);
    return Material ? Material->GetPhysicalMaterial()->SurfaceType : EPhysicalSurface::SurfaceType_Default;
}

TArray<UTDS_StateEffect*> ATDSEnvironmentStructure::GetAllCurrentEffects() const
{
    return Effects;
}

void ATDSEnvironmentStructure::RemoveEffect_Implementation(UTDS_StateEffect* RemoveEffect)
{
    Effects.Remove(RemoveEffect);

    if (!RemoveEffect->bIsAutoDestroyEffect)
    {
        SwitchEffect(RemoveEffect, false);
        EffectRemove = RemoveEffect;
    }
}

void ATDSEnvironmentStructure::AddEffect_Implementation(UTDS_StateEffect* NewEffect)
{
    Effects.Add(NewEffect);

    if (!NewEffect->bIsAutoDestroyEffect)
    {
        SwitchEffect(NewEffect, true);
        EffectAdd = NewEffect;
    }
    else
    {
        if (NewEffect->ParticleEffect)
        {
            ExecuteEffectAdded_OnServer(NewEffect->ParticleEffect);
        }
    }
}

void ATDSEnvironmentStructure::EffectAdd_OnRep()
{
    if (!EffectAdd) return;
    SwitchEffect(EffectAdd, true);
}

void ATDSEnvironmentStructure::EffectRemove_OnRep()
{
    if (!EffectRemove) return;
    SwitchEffect(EffectRemove, false);
}

void ATDSEnvironmentStructure::SwitchEffect(UTDS_StateEffect* Effect, bool bIsAdd)
{
    if (bIsAdd)
    {
        if (Effect && Effect->ParticleEffect)
        {
            FName NameBoneToAttached = Effect->NameBone;
            FVector Location = OffcetEffect;

            const auto SceneComponent = GetRootComponent();
            if (SceneComponent)
            {
                const auto PSystemComponent = UGameplayStatics::SpawnEmitterAttached(Effect->ParticleEffect, SceneComponent,
                    NameBoneToAttached, Location, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
                ParticleSystemEffects.Add(PSystemComponent);
            }
        }
    }
    else
    {
        int32 i = 0;
        bool bIsFind = false;
        if (ParticleSystemEffects.Num() > 0)
        {
            while (i < ParticleSystemEffects.Num() && !bIsFind)
            {
                if (ParticleSystemEffects[i]->Template && Effect->ParticleEffect &&
                    Effect->ParticleEffect == ParticleSystemEffects[i]->Template)
                {
                    bIsFind = true;
                    ParticleSystemEffects[i]->DeactivateSystem();
                    ParticleSystemEffects[i]->DestroyComponent();
                    ParticleSystemEffects.RemoveAt(i);
                }

                ++i;
            }
        }
    }
}

void ATDSEnvironmentStructure::ExecuteEffectAdded_OnServer_Implementation(UParticleSystem* ExecuteFX)
{
    ExecuteEffectAdded_Multicast(ExecuteFX);
}

void ATDSEnvironmentStructure::ExecuteEffectAdded_Multicast_Implementation(UParticleSystem* ExecuteFX)
{
    UTDSTypes::ExecuteEffectAdded(ExecuteFX, this, OffcetEffect, NAME_None);
}

bool ATDSEnvironmentStructure::ReplicateSubobjects(UActorChannel* Channel, FOutBunch* Bunch, FReplicationFlags* RepFlags)
{
    bool Wrote = Super::ReplicateSubobjects(Channel, Bunch, RepFlags);

    for (int32 i = 0; i < Effects.Num(); i++)
    {
        if (Effects.IsValidIndex(i) && Effects[i])
        {
            Wrote |= Channel->ReplicateSubobject(Effects[i], *Bunch,
                *RepFlags);  // (this makes those subobjects 'supported', and from here on those objects may have reference replicated)
        }
    }

    return Wrote;
}

void ATDSEnvironmentStructure::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);

    DOREPLIFETIME(ATDSEnvironmentStructure, Effects);
    DOREPLIFETIME(ATDSEnvironmentStructure, EffectAdd);
    DOREPLIFETIME(ATDSEnvironmentStructure, EffectRemove);
}